package group12.finalproject;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;

import com.facebook.FacebookSdk;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class WelcomePageActivity extends Activity {

    private String user_id;
    AlertDialog alertDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        getWindow().requestFeature(Window.FEATURE_ACTION_BAR);
        Firebase.setAndroidContext(this);
        Bundle extras = getIntent().getExtras();
        user_id = extras.getString(LoginActivity.USERID);


        setContentView(R.layout.activity_welcome_page);
//        Toast.makeText(getApplicationContext(),"user id: "+user_id,Toast.LENGTH_SHORT).show();

        Firebase myFirebaseRef = new Firebase("https://amber-fire-1389.firebaseio.com/").child(user_id);
        myFirebaseRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                for(DataSnapshot more: snapshot.getChildren()){
                    Boolean quizzable = (Boolean) more.child("mQuizzable").getValue();
                    if(quizzable == false){
                        continue;
                    }
                    else{
                        Date currDate = new Date();
                        String quizTaken = (String) more.child("mLastQuizDate").getValue();
                        SimpleDateFormat df = new SimpleDateFormat("MMMM d, yyyy HH:mm:ss a");
                        long timeDifference = 0;
                        Double quizScore = (Double) more.child("mScore").getValue();
                        long scoreTime = 0;
                        try {
                             timeDifference = currDate.getTime() - df.parse(quizTaken).getTime();
                        }
                        catch (ParseException e){
                            System.out.println("Didnt work");
                        }

                        if(0<=quizScore && quizScore<=69){
                            scoreTime = 0;
                        }
                        else if(70<=quizScore && quizScore<=79){
                            scoreTime = 10;
                        }
                        else if(80<=quizScore && quizScore<=89){
                            scoreTime = 30;
                        }
                        else{
                            scoreTime = 48;
                        }




                        if (TimeUnit.MILLISECONDS.toHours(timeDifference) >= (24+scoreTime)){
                            alertDialog = new AlertDialog.Builder(WelcomePageActivity.this).create();
                            alertDialog.setTitle("Study Reminder");
                            alertDialog.setMessage("Hey, it looks like one or more of the flashcard sets you enabled study reminders for should be reviewed! Try brushing up on some flashcards sets you may not have looked over in a while and quiz yourself!");
                            alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            dialog.dismiss();
                                        }
                                    });

                                alertDialog.show();


                        }
                        break;
                    }
                }
            }
            @Override
            public void onCancelled(FirebaseError firebaseError) {
                System.out.println("The read failed: " + firebaseError.getMessage());
            }
        });

        final Button reviewButton = (Button) findViewById(R.id.button_review);
        reviewButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(WelcomePageActivity.this, CardSetActivity.class);
                i.putExtra(LoginActivity.USERID, user_id);
//                startActivity(i);
                startActivityForResult(i, 0);
            }
        });

        final Button newSetButton = (Button) findViewById(R.id.button_new_set);
        newSetButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(WelcomePageActivity.this, CreateSetActivity.class);
                i.putExtra(LoginActivity.USERID, user_id);
                startActivityForResult(i,0);
            }
        });

        final Button connectButton = (Button) findViewById(R.id.button_connect);
        connectButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //TODO
            }
        });


    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if(alertDialog != null) {
            alertDialog.dismiss();
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 0) {
            if (resultCode == RESULT_OK) {

            }
        }
    }
}
