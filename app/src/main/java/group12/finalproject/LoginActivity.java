package group12.finalproject;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.Profile;
import com.facebook.ProfileTracker;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.firebase.client.AuthData;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;

/**
 * Created by Delante on 5/4/2016.
 */
public class LoginActivity extends Activity {

    public static final String USERID = "user_id";
    CallbackManager callbackManager;
    private Firebase FirebaseRef;
    private ProfileTracker mProfileTracker;
    private  AccessTokenTracker accessTokenTracker;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        Firebase.setAndroidContext(this);
        callbackManager = CallbackManager.Factory.create();

        mProfileTracker = new ProfileTracker() {
            @Override
            protected void onCurrentProfileChanged(Profile profile, Profile profile2) {
                updateUI(); //this is the third piece of code I will discuss below
            }
        };
        accessTokenTracker = new AccessTokenTracker() {
            @Override
            protected void onCurrentAccessTokenChanged(AccessToken oldAccessToken,
                                                       AccessToken currentAccessToken) {
                if (currentAccessToken == null) {

                    Intent intent = new Intent(LoginActivity.this, LoginActivity.class);
                    startActivity(intent);

                }
            }
        };


        mProfileTracker.startTracking();
        setContentView(R.layout.activity_login);

        final LoginButton loginButton = (LoginButton) findViewById(R.id.login_button);
        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                // App code
                onFacebookAccessTokenChange(loginResult.getAccessToken());
                accessTokenTracker.startTracking();
            }

            @Override
            public void onCancel() {
                // App code
            }

            @Override
            public void onError(FacebookException exception) {
                // App code
            }
        });

        FirebaseRef = new Firebase(getResources().getString(R.string.firebase_url));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mProfileTracker.stopTracking();
        accessTokenTracker.stopTracking();
    }

    @Override
    public void onResume(){
        super.onResume();
        if(AccessToken.getCurrentAccessToken() != null){
            onFacebookAccessTokenChange(AccessToken.getCurrentAccessToken());
        }
    }

    private void updateUI(){

        boolean enableButtons = AccessToken.getCurrentAccessToken() != null;

        Profile profile = Profile.getCurrentProfile();
/*        if (profile == null) {
            Log.e("Profile", "null");
        }
        if (enableButtons && profile != null) {
            Log.e("Access Token",AccessToken.getCurrentAccessToken().toString());
            Log.e("TabSocial", profile.getName());
        } */
    }

    private void onFacebookAccessTokenChange(AccessToken token) {
        if (token != null) {
            FirebaseRef.authWithOAuthToken("facebook", token.getToken(), new Firebase.AuthResultHandler() {
                @Override
                public void onAuthenticated(AuthData authData) {
                    Intent intent = new Intent(LoginActivity.this, WelcomePageActivity.class);
                    intent.putExtra(USERID,authData.getUid());
                    startActivity(intent);
                }
                @Override
                public void onAuthenticationError(FirebaseError firebaseError) {
                    // there was an error
                }
            });
        } else {
        /* Logged out of Facebook so do a logout from the Firebase app */
            FirebaseRef.unauth();
        }
    }
}
