package group12.finalproject;

import android.app.Dialog;
import android.app.ListActivity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.DataSetObserver;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;
import com.github.amlcurran.showcaseview.ShowcaseView;
import com.github.amlcurran.showcaseview.targets.ViewTarget;

import java.util.HashMap;
import java.util.Map;

import adapters.ShowCardsAdapter;
import models.CardSet;

public class ShowCardsActivity extends ListActivity {

    private Firebase myFirebaseRef;
    private ValueEventListener mConnectedListener;
    private CardSet mCardList;
    private ShowCardsAdapter mAdapter;
    private ListView mListView;
    private String mCardSetName;
    private String mCardSetId;

    private TextView remindersSet;
    private TextView quizScoreText;
    private TextView quizDateText;
    private TextView cardCountText;
    private TextView accuracyText;

    public static final String TERM = "Term";
    public static final String DEFINITION = "Definition";
    private static final String FIREBASE_URL = "https://amber-fire-1389.firebaseio.com/";
    private String TAG = "ShowCardsActivity";
    private String user_id;
    public static final String USERPREF = "UserPref";
    private static final String SHOWCARDSTOUCHED  = "ShowCardsTouched";
    private SharedPreferences userPreference;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_cards);
        Firebase.setAndroidContext(this);
        userPreference = getSharedPreferences(USERPREF, 0);

        mCardList = (CardSet) getIntent().getSerializableExtra(CardSetActivity.CARDSET);
        mCardSetName = getIntent().getStringExtra(CardSetActivity.SETNAME);
        mCardSetId = getIntent().getStringExtra(CardSetActivity.CARDSET_ID);
        user_id = getIntent().getStringExtra(LoginActivity.USERID);

        remindersSet = (TextView) findViewById(R.id.reminderText);
        quizScoreText = (TextView) findViewById(R.id.quizScoreText);
        quizDateText = (TextView) findViewById(R.id.dateQuizzedText);
        cardCountText = (TextView) findViewById(R.id.card_count);
        accuracyText = (TextView) findViewById(R.id.card_accuracy);

        myFirebaseRef = new Firebase(FIREBASE_URL).child(user_id).child(mCardSetId);



        myFirebaseRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                cardCountText.setText("Card Count: " + Long.toString(snapshot.child("mCardSet").getChildrenCount()));

                if ((String) snapshot.child("mLastQuizDate").getValue() != "") {
                    quizDateText.setText("Last Date Quizzed: " + snapshot.child("mLastQuizDate").getValue());
                    quizScoreText.setText("Latest Quiz Score: " + snapshot.child("mScore").getValue() + "%");
                }

                if ((Boolean) snapshot.child("mQuizzable").getValue() == true) {
                    remindersSet.setText("Study Reminders: On");
                } else {
                    remindersSet.setText("Study Reminders: Off");
                }

            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {
                System.out.println("The read failed: " + firebaseError.getMessage());
            }
        });

        mListView = getListView();

        Button addNewCardsButton = (Button) findViewById(R.id.addNewCardsToSet);
        addNewCardsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(ShowCardsActivity.this, CreateCardActivity.class);
                i.putExtra(LoginActivity.USERID, user_id);
                i.putExtra(CardSetActivity.SETNAME, mCardSetName);
                i.putExtra(CardSetActivity.CARDSET_ID, mCardSetId);
                startActivityForResult(i, 0);
            }
        });

        Button quizButton = (Button) findViewById(R.id.quizButton);
        quizButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(ShowCardsActivity.this, QuizActivity.class);
                i.putExtra(LoginActivity.USERID, user_id);
                i.putExtra(CardSetActivity.SETNAME, mCardSetName);
                i.putExtra("cardSet", mCardList);
                i.putExtra(CardSetActivity.CARDSET_ID, mCardSetId);
                startActivity(i);
            }
        });


        mConnectedListener = myFirebaseRef.getRoot().child(".info/connected").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                boolean connected = (Boolean) dataSnapshot.getValue();
                if (connected) {
                    Log.i(TAG, "Connected to Firebase");
                } else {
                    Toast.makeText(ShowCardsActivity.this, "Disconnected from Firebase", Toast.LENGTH_SHORT).show();
                    Log.e(TAG, "Firebase connection error");
                }
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });

        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String term = ((TextView) view.findViewById(R.id.card_term)).getText().toString();

                Intent i = new Intent(ShowCardsActivity.this, SingleCardActivity.class);
//                i.putExtra(TERM, term);
                i.putExtra(CardSetActivity.SETNAME, mCardSetName);
                i.putExtra("cardSet", mCardList);
                i.putExtra("position", position);
                startActivity(i);
            }
        });

        mListView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            private int pos;

            public void dialogPopUp() {
                final Dialog d = new Dialog(ShowCardsActivity.this);
                d.setContentView(R.layout.dialog_edit_card);
                d.setTitle("Edit this card");
                final Button cancelB = (Button) d.findViewById(R.id.dialog_cancel_button);
                final Button submitB = (Button) d.findViewById(R.id.dialog_submit_button);
                cancelB.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        d.dismiss();
                    }
                });
                submitB.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        TextView newTermTV = (TextView) d.findViewById(R.id.edit_card_term);
                        TextView newDefTV = (TextView) d.findViewById(R.id.edit_card_def);
                        String newTerm = newTermTV.getText().toString();
                        String newDef = newDefTV.getText().toString();
                        Map<String, Object> newCard = new HashMap<String, Object>();
                        if (!newTerm.equals(""))
                            newCard.put("term", newTerm);
                        if (!newDef.equals(""))
                            newCard.put("definition", newDef);
                        if (!newCard.isEmpty())
                            myFirebaseRef.child("mCardSet").child(Integer.toString(pos)).updateChildren(newCard);
                        d.dismiss();
                    }
                });
                d.show();

            }

            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                pos = position;
                dialogPopUp();
                return true;
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();

        mAdapter = new ShowCardsAdapter(myFirebaseRef.child("mCardSet"), R.layout.card_overview, this);
        mListView.setAdapter(mAdapter);
        mAdapter.registerDataSetObserver(new DataSetObserver() {
            private int changeCount = 0;

            @Override
            public void onChanged() {
                super.onChanged();
                mListView.setSelection(mAdapter.getCount() - 1);

                if (changeCount == 0) {
                    if(!userPreference.contains(SHOWCARDSTOUCHED)) {
                        userPreference.edit().putBoolean(SHOWCARDSTOUCHED, true).apply();
                        new ShowcaseView.Builder(ShowCardsActivity.this)
                                .setTarget(new ViewTarget(mListView))
                                .setContentTitle("Edit card set name")
                                .setContentText("Long click the card set to edit")
                                .setShowcaseDrawer(new CardSetActivity.CustomShowcaseView(ShowCardsActivity.this.getResources(), mListView))
                                .build();
                    }
                }
                changeCount += 1;

            }
        });
    }

    @Override
    public void onStop() {
        super.onStop();
        myFirebaseRef.getRoot().child(".info/connected").removeEventListener(mConnectedListener);
        mAdapter.cleanup();
    }

}
