package group12.finalproject;


import android.app.ActionBar;
import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.client.Firebase;

import java.util.ArrayList;
import java.util.List;

import models.Card;
import models.CardSet;

public class CreateSetActivity extends Activity {
    private LinearLayout termList;
    private Firebase myFirebaseRef;
    private String user_id;
    private CheckBox studyReminderCheckbox;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.create_set);

        user_id = getIntent().getStringExtra(LoginActivity.USERID);
        termList = (LinearLayout)findViewById(R.id.termList);
        studyReminderCheckbox = (CheckBox) findViewById(R.id.newSetStudyReminderCheckbox);


        Firebase.setAndroidContext(this);
        myFirebaseRef = new Firebase("https://amber-fire-1389.firebaseio.com/");


        Button addNewTerm = (Button)findViewById(R.id.addTerm);
        View.OnClickListener toAddTerm = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addNewTermRow();
            }

        };
        addNewTerm.setOnClickListener(toAddTerm);

        Button addNewSet = (Button)findViewById(R.id.createSet);
        View.OnClickListener toAddSet = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createNewSet();
            }

        };
        addNewSet.setOnClickListener(toAddSet);
    }

    public void addNewTermRow() {
        LinearLayout newRow = new LinearLayout(this);
        newRow.setOrientation(LinearLayout.HORIZONTAL);
        LinearLayout.LayoutParams rowParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        rowParams.weight = (float)0.02;
        newRow.setLayoutParams(rowParams);

        TextView termView = new TextView(this);
        termView.setText("Term:");
        termView.setLayoutParams(new ActionBar.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        termView.setTextAppearance(this, android.R.style.TextAppearance_Medium);

        EditText termEdit = new EditText(this);
        LinearLayout.LayoutParams editParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        editParams.weight = (float)0.02;
        termEdit.setEms(2);
        termEdit.setLayoutParams(editParams);

        TextView defView = new TextView(this);
        defView.setText("Definition:");
        defView.setLayoutParams(new ActionBar.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        defView.setTextAppearance(this, android.R.style.TextAppearance_Medium);

        EditText defEdit = new EditText(this);
        defEdit.setEms(4);
        defEdit.setLayoutParams(editParams);

        newRow.addView(termView);
        newRow.addView(termEdit);
        newRow.addView(defView);
        newRow.addView(defEdit);

        termList.addView(newRow);
    }

    public void createNewSet() {
        List<Card> cards = new ArrayList<Card>();

        EditText setNameEdit = (EditText)findViewById(R.id.editSetName);
        String setName = setNameEdit.getText().toString();

        if (setName.matches("")) {
            Toast.makeText(this, "No Set Name Entered", Toast.LENGTH_SHORT).show();
            return;
        }

        // look through outer vertical LinearLayout
        for (int i=0; i < termList.getChildCount(); i++) {
            if (termList.getChildAt(i) instanceof LinearLayout) {
                LinearLayout inner = (LinearLayout) termList.getChildAt(i);

                // get EditText elements from inner horizontal LinearLayout
                EditText termEdit = (EditText) inner.getChildAt(1);
                EditText defEdit = (EditText) inner.getChildAt(3);

                String term = termEdit.getText().toString();
                String def = defEdit.getText().toString();

                // don't add card if there is no term
                if (term.matches("")) {
                    continue;
                }

                cards.add(new Card(term, def, 0L, 0L));

            }
        }

        CardSet cardSet = new CardSet(cards, 0, setName, "", studyReminderCheckbox.isChecked());

        if (cards.isEmpty()) {
            Toast.makeText(this, "Please enter at least one term", Toast.LENGTH_SHORT).show();
        } else {
            myFirebaseRef.child(user_id).push().setValue(cardSet);
            Toast.makeText(getApplicationContext(),"Set Created Successfully!", Toast.LENGTH_LONG).show();
            setResult(Activity.RESULT_OK);
            finish();
        }
    }
}
