package group12.finalproject;

import android.app.Dialog;
import android.app.ListActivity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.database.DataSetObserver;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.RectF;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;
import com.github.amlcurran.showcaseview.ShowcaseDrawer;
import com.github.amlcurran.showcaseview.ShowcaseView;
import com.github.amlcurran.showcaseview.targets.ViewTarget;

import adapters.CardSetAdapter;
import models.CardSet;

public class CardSetActivity extends ListActivity {
    private Firebase myFirebaseRef;
    private String user_id;
    private ValueEventListener mConnectedListener;
    private CardSetAdapter mAdapter;
    private ListView mListView;
    private static final String FIREBASE_URL = "https://amber-fire-1389.firebaseio.com/";
    private static final String TAG = "CardSetActivity";
    public static final String CARDSET = "CardSet";
    public static final String SETNAME = "setName";
    public static final String CARDSET_ID = "CardSetId";
    public static final String USERPREF = "UserPref";
    private static final String CARDSETTOUCHED = "CardSetTouched";
    private SharedPreferences userPreference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_card_set);
        userPreference = getSharedPreferences(USERPREF, 0);
        mListView = getListView();
        user_id = getIntent().getStringExtra(LoginActivity.USERID);
        Firebase.setAndroidContext(this);
        myFirebaseRef = new Firebase(FIREBASE_URL).child(user_id);

        mConnectedListener = myFirebaseRef.getRoot().child(".info/connected").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                boolean connected = (Boolean) dataSnapshot.getValue();
                if (connected) {
                    Log.i(TAG, "Connected to Firebase");
                } else {
                    Toast.makeText(CardSetActivity.this, "Disconnected from Firebase", Toast.LENGTH_SHORT).show();
                    Log.i(TAG, "Disconnected from Firebase");
                }
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {
                Log.e(TAG, "FirebaseError: " + firebaseError.toString());
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();

        //TODO uncomment this to test one-time UI hint
//        if (userPreference.contains(CARDSETTOUCHED)) {
//            userPreference.edit().remove(CARDSETTOUCHED).commit();
//        }
        mAdapter = new CardSetAdapter(myFirebaseRef, R.layout.card_set_overview, this);
        mListView.setAdapter(mAdapter);
        mAdapter.registerDataSetObserver(new DataSetObserver() {
            private int changeCount = 0;

            @Override
            public void onChanged() {
                super.onChanged();
                mListView.setSelection(mAdapter.getCount() - 1);
                if (changeCount == 0) {

                    if (!userPreference.contains(CARDSETTOUCHED)) {
                        userPreference.edit().putBoolean(CARDSETTOUCHED, true).apply();
                        new ShowcaseView.Builder(CardSetActivity.this)
                                .setTarget(new ViewTarget(mListView))
                                .setContentTitle("Edit card set name")
                                .setContentText("Long click the card set to edit")
                                .setShowcaseDrawer(new CustomShowcaseView(CardSetActivity.this.getResources(), mListView))
                                .build();
                    }
                    Log.d(TAG, "this activity touched: " + userPreference.getBoolean(CARDSETTOUCHED, false));
                }
                changeCount += 1;
            }


        });

        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                CardSet cardSet = (CardSet) parent.getAdapter().getItem(position);
                String cardSetName = ((TextView) view.findViewById(R.id.card_set_name)).getText().toString();
                String cardSetId = mAdapter.getItemKey(position);
                Intent i = new Intent(CardSetActivity.this, ShowCardsActivity.class);
                i.putExtra(CARDSET_ID, cardSetId);
                i.putExtra(CARDSET, cardSet);
                i.putExtra(SETNAME, cardSetName);
                i.putExtra(LoginActivity.USERID, user_id);
                startActivity(i);
            }

        });

        /*
        *  Edit card
        * */
        mListView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            private String mSetKey;

            public void dialogPopUp() {
                final Dialog d = new Dialog(CardSetActivity.this);
                d.setContentView(R.layout.dialog_edit_set_name);
                d.setTitle("Edit Card Set Name");

                final Button cancelB = (Button) d.findViewById(R.id.dialog_cancel_button);
                final Button submitB = (Button) d.findViewById(R.id.dialog_submit_button);
                cancelB.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        d.dismiss();
                    }
                });
                submitB.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        TextView newSetNameTV = (TextView) d.findViewById(R.id.new_set_name);
                        String newSetName = newSetNameTV.getText().toString();
                        String setKey = mSetKey;
                        if (!newSetName.equals(""))
                            myFirebaseRef.child(setKey).child("mSetName").setValue(newSetName);
                        else
                            Toast.makeText(getApplication(), "No change is done!", Toast.LENGTH_SHORT).show();
                        d.dismiss();
                    }
                });
                d.show();
            }

            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                mSetKey = mAdapter.getItemKey(position);
                dialogPopUp();
                return true;
            }
        });


    }

    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public void onStop() {
        super.onStop();
        myFirebaseRef.getRoot().child(".info/connected").removeEventListener(mConnectedListener);
        mAdapter.cleanup();
    }

    protected static class CustomShowcaseView implements ShowcaseDrawer {

        private final float width;
        private final float height;
        private final Paint eraserPaint;
        private final Paint basicPaint;
        private final int eraseColour;
        private final RectF renderRect;

        public CustomShowcaseView(Resources resources, View itemView) {
//            width = resources.getDimension(R.dimen.custom_showcase_width);
//            height = resources.getDimension(R.dimen.custom_showcase_height);
            width = itemView.getWidth();
            height = itemView.getHeight();
            PorterDuffXfermode xfermode = new PorterDuffXfermode(PorterDuff.Mode.MULTIPLY);
            eraserPaint = new Paint();
            eraserPaint.setColor(0xFFFFFF);
            eraserPaint.setAlpha(30);
            eraserPaint.setXfermode(xfermode);
            eraserPaint.setAntiAlias(true);
            eraseColour = resources.getColor(R.color.custom_showcase_bg);
            basicPaint = new Paint();
            renderRect = new RectF();
        }

        @Override
        public void setShowcaseColour(int color) {
            eraserPaint.setColor(color);
        }

        @Override
        public void drawShowcase(Bitmap buffer, float x, float y, float scaleMultiplier) {
            Canvas bufferCanvas = new Canvas(buffer);
            renderRect.left = x - width / 2f;
            renderRect.right = x + width / 2f;
            renderRect.top = y - height / 2f;
            renderRect.bottom = y + height / 2f;
            bufferCanvas.drawRect(renderRect, eraserPaint);
        }

        @Override
        public int getShowcaseWidth() {
            return (int) width;
        }

        @Override
        public int getShowcaseHeight() {
            return (int) height;
        }

        @Override
        public float getBlockedRadius() {
            return width;
        }

        @Override
        public void setBackgroundColour(int backgroundColor) {
            // No-op, remove this from the API?
        }

        @Override
        public void erase(Bitmap bitmapBuffer) {
            bitmapBuffer.eraseColor(eraseColour);
        }

        @Override
        public void drawToCanvas(Canvas canvas, Bitmap bitmapBuffer) {
            canvas.drawBitmap(bitmapBuffer, 0, 0, basicPaint);
        }

    }
}
