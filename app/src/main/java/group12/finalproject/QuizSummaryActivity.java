package group12.finalproject;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;


/**
 * Created by derekferrell on 5/8/16.
 */
public class QuizSummaryActivity extends Activity {

    private String mCardSetName;
    private String mTime;
    private int mNumQuestions;
    private int mNumCorrect;
    private double mScore;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz_summary);

        mCardSetName = getIntent().getStringExtra("setName");
        mTime = getIntent().getStringExtra("time");
        mNumQuestions = getIntent().getIntExtra("numQuestions", 0);
        mNumCorrect = getIntent().getIntExtra("numCorrect", 0);
        mScore = getIntent().getDoubleExtra("score", 0);

        TextView setName = (TextView) findViewById(R.id.setName);
        setName.setText("Card Set: " + mCardSetName);

        TextView timeTaken = (TextView) findViewById(R.id.timeTaken);
        timeTaken.setText("Time taken: " + mTime);

        TextView numQuestions = (TextView) findViewById(R.id.numQuestions);
        numQuestions.setText("Total Questions: " + mNumQuestions);

        TextView numCorrect = (TextView) findViewById(R.id.numCorrect);
        numCorrect.setText("Correct Answers: " + mNumCorrect);

        TextView score = (TextView) findViewById(R.id.scoreView);
        score.setText("Score: " + mScore + "%");

        final Button backButton = (Button) findViewById(R.id.buttonBackToSet);
        backButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                final Intent intent = NavUtils.getParentActivityIntent(QuizSummaryActivity.this);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                NavUtils.navigateUpTo(QuizSummaryActivity.this, intent);
            }
        });
    }
}
