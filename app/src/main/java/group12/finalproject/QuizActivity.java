package group12.finalproject;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.MutableData;
import com.firebase.client.Transaction;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

import models.Card;
import models.CardSet;

/**
 * Created by derekferrell on 5/8/16.
 */
public class QuizActivity extends Activity {

    private String mUserId;
    private String mCardSetName;
    private CardSet mCardSet;
    private List<Card> mCardList;
    private String mCardSetId;
    private int position = 0;
    private int listLength;
    private int numCorrect = 0;
    private int numAnswered = 0;
    private String curAnswer = "";
    private Firebase myFirebaseRef;
    private static final String FIREBASE_URL = "https://amber-fire-1389.firebaseio.com/";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz);

        Firebase.setAndroidContext(this);

        mUserId = getIntent().getStringExtra(LoginActivity.USERID);
        mCardSetName = getIntent().getStringExtra(CardSetActivity.SETNAME);
        mCardSet = (CardSet) getIntent().getSerializableExtra("cardSet");
        mCardSetId = getIntent().getStringExtra(CardSetActivity.CARDSET_ID);
        mCardList = mCardSet.getmCardSet();
        listLength = mCardList.size();
        myFirebaseRef = new Firebase("https://amber-fire-1389.firebaseio.com/").child(mUserId).child(mCardSetId);

        buildQuestion(position);

        LinearLayout optionsList = (LinearLayout)findViewById(R.id.optionsList);
        optionsList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i("infos", "" + v.getId());
            }
        });
    }

    private void buildQuestion(int position) {
        Card current = mCardList.get(position);
        String curTerm = current.getTerm();
        String curDef = current.getDefinition();
        curAnswer = curDef;
        String wrong1, wrong2 = "";
        String[] options;

        if (position + 1 < listLength) {
            wrong1 = mCardList.get(position + 1).getDefinition();
        } else {
            wrong1 = mCardList.get(0).getDefinition();
        }

        if (position + 2 < listLength) {
            wrong2 = mCardList.get(position + 2).getDefinition();
        } else if (wrong1.equals(mCardList.get(0).getDefinition())) {
            wrong2 = mCardList.get(1).getDefinition();
        } else {
            wrong2 = mCardList.get(0).getDefinition();
        }

        options = new String[]{curDef, wrong1, wrong2};
        shuffleArray(options);

        TextView termView = (TextView) findViewById(R.id.termView);
        termView.setText(curTerm);

        TextView option1 = (TextView) findViewById(R.id.option1);
        option1.setText(options[0]);

        TextView option2 = (TextView) findViewById(R.id.option2);
        option2.setText(options[1]);

        TextView option3 = (TextView) findViewById(R.id.option3);
        option3.setText(options[2]);

    }

    public void onOptionClick(View v) {
        TextView selected = (TextView) v;
        String answer = (String)selected.getText();

        if (answer.equals(curAnswer)) {
            Toast.makeText(QuizActivity.this, "correct!", Toast.LENGTH_SHORT).show();
            numCorrect++;
            myFirebaseRef.child("mCardSet").child(Integer.toString(position)).child("numCorrect").runTransaction(new Transaction.Handler() {
                @Override
                public Transaction.Result doTransaction(MutableData currData) {
                    if (currData.getValue() == null)
                        currData.setValue(0);
                    else
                        currData.setValue((Long) currData.getValue() + 1);
                    return Transaction.success(currData);
                }

                @Override
                public void onComplete(FirebaseError firebaseError, boolean b, DataSnapshot dataSnapshot) {
                    // no op
                }
            });
        } else {
            Toast.makeText(QuizActivity.this, "incorrect.", Toast.LENGTH_SHORT).show();
            myFirebaseRef.child("mCardSet").child(Integer.toString(position)).child("numWrong").runTransaction(new Transaction.Handler() {
                @Override
                public Transaction.Result doTransaction(MutableData currData) {
                    if (currData.getValue() == null)
                        currData.setValue(0);
                    else
                        currData.setValue((Long) currData.getValue() + 1);

                    return Transaction.success(currData);
                }

                @Override
                public void onComplete(FirebaseError firebaseError, boolean b, DataSnapshot dataSnapshot) {
                    // no op
                }
            });
        }

        numAnswered++;

        if (position+1 < listLength) {
            position++;
            buildQuestion(position);
        } else {
            Toast.makeText(QuizActivity.this, "correct: " + numCorrect + " total: " + numAnswered, Toast.LENGTH_SHORT).show();
            double score = ((double)numCorrect/(double)numAnswered)*100;
            String ts = DateFormat.getDateTimeInstance().format(new Date());
            NumberFormat formatter = new DecimalFormat("#0.00");

            myFirebaseRef.child("mScore").setValue(Double.parseDouble(formatter.format(score)));
            myFirebaseRef.child("mLastQuizDate").setValue(ts);

            Intent i = new Intent(QuizActivity.this, QuizSummaryActivity.class);
            i.putExtra("setName", mCardSetName);
            i.putExtra("time", ts);
            i.putExtra("numQuestions", listLength);
            i.putExtra("numCorrect", numCorrect);
            i.putExtra("score", score);
            startActivity(i);
        }
    }

    static void shuffleArray(String[] ar) {
        Random rnd = ThreadLocalRandom.current();
        for (int i = ar.length - 1; i > 0; i--) {
            int index = rnd.nextInt(i + 1);
            String a = ar[index];
            ar[index] = ar[i];
            ar[i] = a;
        }
    }
}
