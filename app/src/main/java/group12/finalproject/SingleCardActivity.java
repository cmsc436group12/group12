package group12.finalproject;

import android.app.ActionBar;
import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.NavUtils;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.client.Firebase;
import com.firebase.client.ValueEventListener;
import com.github.amlcurran.showcaseview.ShowcaseView;
import com.github.amlcurran.showcaseview.targets.ViewTarget;

import java.util.List;

import models.Card;
import models.CardSet;

public class SingleCardActivity extends Activity
        implements FragmentManager.OnBackStackChangedListener {

    private Firebase myFirebaseRef;
    private ValueEventListener mConnectedListener;
    private boolean mShowingBack = false;
    private Handler mHandler = new Handler();
    private String mTerm;
    private String mDefinition;
    private String mCardSetName;
    private CardSet mCardSet;
    private List<Card> mCardList;
    private int position;
    private Card mCard;
    private int listLength;
    private static final String FIREBASE_URL = "https://amber-fire-1389.firebaseio.com/";
    private String TAG = "SingleCardActivity";
    private Bundle mArgs;
    private SharedPreferences userPreference;
    private static final String SINGLECARDTOUCHED = "SingleCardTouched";
    public static final String USERPREF = "UserPref";
    private GestureDetector mGestureDetector;
    private Button mPreviousButton;
    private Button mNextButton;
    private static final int SWIPE_MIN_DISTANCE = 120;
    private static final int SWIPE_MAX_OFF_PATH = 250;
    private static final int SWIPE_THRESHOLD_VELOCITY = 200;
    private View mContainerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_single_card);

        userPreference = getSharedPreferences(USERPREF, 0);

        mCardSetName = getIntent().getStringExtra(CardSetActivity.SETNAME);
        mCardSet = (CardSet) getIntent().getSerializableExtra("cardSet");
        mCardList = mCardSet.getmCardSet();
        position = getIntent().getIntExtra("position", 0);
        mCard = mCardList.get(position);
        mTerm = mCard.getTerm();
        mDefinition = mCard.getDefinition();
        listLength = mCardList.size();

        myFirebaseRef = new Firebase(FIREBASE_URL).child("user1").child(mCardSetName).child("mCardSet");
        mArgs = new Bundle();
        mArgs.putSerializable(ShowCardsActivity.TERM, mTerm);
        mArgs.putSerializable(ShowCardsActivity.DEFINITION, mDefinition);

        if (savedInstanceState == null) {
            CardFrontFragment front = new CardFrontFragment();

            front.setArguments(mArgs);
            getFragmentManager().beginTransaction()
                    .add(R.id.container, front)
                    .commit();
        } else {
            mShowingBack = (getFragmentManager().getBackStackEntryCount() > 0);
        }

        getFragmentManager().addOnBackStackChangedListener(this);
        //TODO
        setupGestureDetector();

        FrameLayout container = (FrameLayout)findViewById(R.id.container);

        mNextButton = (Button) findViewById(R.id.nextButton);
        mNextButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (position + 1 >= listLength) {
                    position = 0;
                } else {
                    position++;
                }

                Card nextCard = mCardList.get(position);
                Intent i = new Intent(SingleCardActivity.this, SingleCardActivity.class);
                i.putExtra("Term", nextCard.getTerm());
                i.putExtra("Definition", nextCard.getDefinition());
                i.putExtra(CardSetActivity.SETNAME, mCardSetName);
                i.putExtra("cardSet", mCardSet);
                i.putExtra("position", position);
                startActivity(i);
            }
        });

        mPreviousButton = (Button) findViewById(R.id.previousButton);
        mPreviousButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (position - 1 < 0) {
                    position = listLength - 1;
                } else {
                    position--;
                }

                Card previousCard = mCardList.get(position);
                Intent i = new Intent(SingleCardActivity.this, SingleCardActivity.class);
                i.putExtra("Term", previousCard.getTerm());
                i.putExtra("Definition", previousCard.getDefinition());
                i.putExtra(CardSetActivity.SETNAME, mCardSetName);
                i.putExtra("cardSet", mCardSet);
                i.putExtra("position", position);
                startActivity(i);
            }
        });

        mContainerView = this.findViewById(R.id.container);
        mContainerView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return mGestureDetector.onTouchEvent(event);
            }
        });

        ActionBar actionBar = getActionBar();
        if(actionBar != null) {
            actionBar.setHomeButtonEnabled(false);
            actionBar.setDisplayHomeAsUpEnabled(false);
        }
    }

    @Override
    public void onStart() {
        //TODO uncomment this to test one-time UI hint
//        if (userPreference.contains(SINGLECARDTOUCHED)) {
//            userPreference.edit().remove(SINGLECARDTOUCHED).commit();
//        }
        super.onStart();
        View rootV = this.findViewById(android.R.id.content).getRootView();
        if (!userPreference.contains(SINGLECARDTOUCHED)) {
            userPreference.edit().putBoolean(SINGLECARDTOUCHED, true).apply();
            ShowcaseView scv = new ShowcaseView.Builder(SingleCardActivity.this)
                    .setTarget(new ViewTarget(rootV))
                    .setContentTitle("Browsing cards")
                    .setContentText("To flip the card: tap the card\n" +
                            "To go to next card: swipe left\n" +
                            "To go to previous card: swipe right")
                    .setShowcaseDrawer(new CardSetActivity.CustomShowcaseView(SingleCardActivity.this.getResources(), rootV))
                    .blockAllTouches()
                    .build();
        }
    }

    @Override
    public void onBackPressed() {
        final Intent intent = NavUtils.getParentActivityIntent(SingleCardActivity.this);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        NavUtils.navigateUpTo(SingleCardActivity.this, intent);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_flip:
                flipCard();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackStackChanged() {
        mShowingBack = (getFragmentManager().getBackStackEntryCount() > 0);
        // When the back stack changes, invalidate the options menu (action bar).
        invalidateOptionsMenu();
    }

    public static class CardFrontFragment extends Fragment {

        private String term;

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            Bundle args = getArguments();
            String term = (String) args.getSerializable(ShowCardsActivity.TERM);

            View cardFront = inflater.inflate(R.layout.fragment_card_front, container, false);
            TextView cardFrontText = (TextView) cardFront.findViewById(R.id.card_front_text);

            cardFrontText.setText(term);
            return cardFront;
        }
    }

    public static class CardBackFragment extends Fragment {

        private String def;

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            Bundle args = getArguments();
            String def = (String) args.getSerializable(ShowCardsActivity.DEFINITION);

            View cardBack = inflater.inflate(R.layout.fragment_card_back, container, false);
            TextView cardFrontText = (TextView) cardBack.findViewById(R.id.card_back_text);
            cardFrontText.setText(def);
            return cardBack;
        }
    }

    private void flipCard() {
        if (mShowingBack) {
            getFragmentManager().popBackStack();
            return;
        }


        mShowingBack = true;
        CardBackFragment back = new CardBackFragment();
        back.setArguments(mArgs);
        getFragmentManager().beginTransaction()
                .setCustomAnimations(
                        R.animator.card_flip_right_in,
                        R.animator.card_flip_right_out,
                        R.animator.card_flip_left_in,
                        R.animator.card_flip_left_out)
                .replace(R.id.container, back)
                .addToBackStack(null)
                .commit();

        mHandler.post(new Runnable() {
            @Override
            public void run() {
                invalidateOptionsMenu();
            }
        });
    }

    private void setupGestureDetector() {
        mGestureDetector = new GestureDetector(this,
                new GestureDetector.SimpleOnGestureListener() {
                    @Override
                    public boolean onDown(MotionEvent event) {
                        return true;
                    }

                    @Override
                    public boolean onFling(MotionEvent e1,
                                           MotionEvent e2, float velocityX, float velocityY) {
                        Log.d(TAG, "onFling");
                        if (Math.abs(e1.getY() - e2.getY()) > SWIPE_MAX_OFF_PATH){
                            return false;
                        }

                        // right to left swipe
                        if (e1.getX() - e2.getX() > SWIPE_MIN_DISTANCE
                                && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
                            Log.d(TAG, "Right to left swipe");
                            mNextButton.performClick();

                        }
                        // left to right swipe
                        else if (e2.getX() - e1.getX() > SWIPE_MIN_DISTANCE
                                && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
                            Log.d(TAG, "Left to Right swipe");
                            mPreviousButton.performClick();
                        }
                        return true;
                    }

                    @Override
                    public boolean onSingleTapConfirmed(MotionEvent event) {
                        flipCard();
                        return true;
                    }
                });
    }

}
