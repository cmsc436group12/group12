package adapters;

import android.app.Activity;
import android.view.View;
import android.widget.TextView;

import com.firebase.client.Query;

import group12.finalproject.R;
import models.Card;

/**
 * Created by York on 5/5/2016.
 */
public class ShowCardsAdapter extends FirebaseListAdapter<Card>{

    public ShowCardsAdapter(Query mRef, int mLayout, Activity activity) {
        super(mRef, Card.class, mLayout, activity);
    }

    @Override
    protected void populateView(View v, Card model, String modelKey) {
        final TextView cardTermView = (TextView) v.findViewById(R.id.card_term);
        final TextView cardAccuracyV = (TextView) v.findViewById(R.id.card_accuracy);

        cardTermView.setText(model.getTerm());
        Double numCorr =  model.getNumCorrect().doubleValue();
        Double numWrong = model.getNumWrong().doubleValue();

        if (numCorr == 0d && numWrong == 0d) {
            cardAccuracyV.setText("Not quizzed yet.");
        } else {
            Long accuracy = Math.round(numCorr / (numCorr + numWrong) * 100d);
            cardAccuracyV.setText("Accuracy: " + accuracy + "%");
        }

    }
}
