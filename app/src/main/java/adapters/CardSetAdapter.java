package adapters;

import android.app.Activity;
import android.view.View;
import android.widget.TextView;

import com.firebase.client.Query;

import java.util.Date;

import group12.finalproject.R;
import models.CardSet;

/**
 * Created by York on 5/3/2016.
 */
public class CardSetAdapter extends FirebaseListAdapter<CardSet>{

    public CardSetAdapter(Query mRef, int mLayout, Activity activity) {
        super(mRef, CardSet.class, mLayout, activity);
    }


    @Override
    protected void populateView(View v, CardSet model, String modelKey) {
        final TextView cardSetNameView = (TextView) v.findViewById(R.id.card_set_name);
//        final TextView cardsCountView = (TextView) v.findViewById(R.id.cards_count);
        final TextView lastQuizDateV = (TextView) v.findViewById(R.id.last_quiz_date);

        String lastQuizD = model.getmLastQuizDate();
        if (lastQuizD.isEmpty()) {
            lastQuizDateV.setText("No quiz attempt yet.");
        } else {
            Date lastQuiz = new Date(lastQuizD);
            String timeDiff = getQuizAge(lastQuiz) + " ago";
            lastQuizDateV.setText("Last quiz: " + timeDiff);
        }
        cardSetNameView.setText(model.getmSetName());
//        cardsCountView.setText(Integer.toString(model.getmCardSet().size()));
    }

    private String getQuizAge(Date dt1) {
        String result = "";
        Date dt2 = new Date();

        long diff = dt2.getTime() - dt1.getTime();
        long diffSeconds = diff / 1000 % 60;
        long diffMinutes = diff / (60 * 1000) % 60;
        long diffHours = diff / (60 * 60 * 1000);
        int diffInDays = (int) ((dt2.getTime() - dt1.getTime()) / (1000 * 60 * 60 * 24));

        if (diffInDays >= 1) {
            result = Integer.toString(diffInDays);
            result += diffInDays == 1? " day" : " days";
        } else if (diffHours >= 1) {
            result = Long.toString(diffHours);
            result += diffHours == 1? " hour" : " hours";
        } else if (diffMinutes >= 1) {
            result = Long.toString(diffMinutes);
            result += diffMinutes == 1? " minute" : " minutes";
        } else {
            result = "less than 1 minute";
        }

        return result;
    }
}