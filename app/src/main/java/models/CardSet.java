package models;

import android.provider.ContactsContract;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * Created by York on 5/3/2016.
 */
public class CardSet implements Serializable{

    private double mScore = 0;
    private String mSetName;
    private String mLastQuizDate;
    private Boolean mQuizzable;
    private List<Card> mCardSet = new ArrayList<Card>();

    @SuppressWarnings("unused")
    private CardSet() {

    }

    public CardSet(List<Card> cardSet, double score, String setName, String lastQuizDate, Boolean quizzable) {
        mScore = score;
        mCardSet = cardSet;
        mSetName = setName;
        mQuizzable = quizzable;   // default: true
        mLastQuizDate = lastQuizDate;  // default: ""
    }

    public String getmSetName() {
        return mSetName;
    }

    public String getmLastQuizDate() {
        return mLastQuizDate;
    }

    public Boolean getmQuizzable() {
        return mQuizzable;
    }

    public double getmScore() {
        return mScore;
    }

    public List<Card> getmCardSet() {
        return mCardSet;
    }

    public String toString () {
        return mCardSet.toString();
    }
}
