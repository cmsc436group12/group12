package models;

import java.io.Serializable;
import java.util.List;

/**
 * Created by York on 5/3/2016.
 */
public class User implements Serializable{
    private List<CardSet> cardSets;
//    private String user_id;

    public User () {

    }

    public User(List<CardSet> c, String id) {
        cardSets = c;
//        user_id = id;
    }

//    public String getUser_id() {
//        return user_id;
//    }

    public List<CardSet> getCardSets() {
        return cardSets;
    }
}
