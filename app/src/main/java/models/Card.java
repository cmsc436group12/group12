package models;

import java.io.Serializable;
import java.lang.ref.SoftReference;
import java.util.Map;

/**
 * Created by York on 5/2/2016.
 */
public class Card implements Serializable{

    private String term;
    private String definition;
    private Long numCorrect;
    private Long numWrong;

    @SuppressWarnings("unused")
    private Card() {

    }

        public Card(String term, String definition, Long numCorrect, Long numWrong) {
        this.definition = definition;
        this.term = term;
        this.numCorrect = numCorrect;
        this.numWrong = numWrong;
    }

    public Long getNumCorrect() {
        return numCorrect;
    }

    public Long getNumWrong() {
        return numWrong;
    }

    public String getTerm() {
        return term;
    }

    public String getDefinition() {
        return definition;
    }

    public String toString() {
        return this.term + " -> " + this.definition;
    }
}
